package com.founder.schedule;

import com.founder.config.XxlJobConfig;
import com.founder.core.domain.PayChannel;
import com.founder.core.log.MyLog;
import com.founder.service.FianceAliService;
import com.founder.service.FianceWxService;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class FianceSchedule {

    private final MyLog _log = MyLog.getLog(FianceSchedule.class);

    @Autowired
    XxlJobConfig xxlJobConfig;

    @Autowired
    FianceWxService fianceWxService;

    @Autowired
    FianceAliService fianceAliService;

    @Scheduled(cron = "${config.cron.download-wx}")
    public void downloadWx(){
        String channelName = "WX";
        _log.info("时间到，开始下载微信对账单：{}", channelName);
        List<PayChannel> list = fianceWxService.findPayChannel(channelName);
        download(list);
    }

    @Scheduled(cron = "${config.cron.download-wx-check}")
    public void downloadWxCheck(){
        String channelName = "WX";
        _log.info("时间到，开始检查微信对账单：{}", channelName);
        List<PayChannel> list = fianceWxService.findPayChannel(channelName);
        download(check(list));
    }

    @Scheduled(cron = "${config.cron.download-ali}")
    public void downloadAli(){
        String channelName = "ALIPAY";
        _log.info("时间到，开始下载支付宝对账单：{}", channelName);
        List<PayChannel> list = fianceAliService.findPayChannel(channelName);
        download(list);
    }

    @Scheduled(cron = "${config.cron.download-ali-check}")
    public void downloadAliCheck(){
        String channelName = "ALIPAY";
        _log.info("时间到，开始检查支付宝对账单：{}", channelName);
        List<PayChannel> list = fianceAliService.findPayChannel(channelName);
        download(check(list));
    }

    private void download(List<PayChannel> list){
        String billDate = DateFormatUtils.format(DateUtils.addDays(new Date(), -1),"yyyyMMdd");
        _log.info("账单日期：{}", billDate);
        list = list.stream().filter(x->x.getState() == 1).collect(Collectors.toList());
        for (int i = 0; i < list.size(); i++) {
            PayChannel payChannel = list.get(i);
            _log.info("[{}]开始{}下载{}对账单{}", i, billDate, list.get(i).getRemark(),list.get(i).getMchId());
            if ("ALIPAY".equals(payChannel.getChannelName())){
                fianceAliService.downloadBill(payChannel.getMchId(),billDate,payChannel.getChannelId());
            } else if ("WX".equals(payChannel.getChannelName())){
                fianceWxService.downloadBill(payChannel.getMchId(),billDate,payChannel.getChannelId());
            }
            _log.info("下载完成");
        }
    }

    private List<PayChannel> check(List<PayChannel> list){
        List<PayChannel> channelList = new ArrayList<>();
        String billDate = DateFormatUtils.format(DateUtils.addDays(new Date(), -1),"yyyyMMdd");
        list = list.stream().filter(x->x.getState() == 1).collect(Collectors.toList());
        for (int i = 0; i < list.size(); i++) {
            PayChannel payChannel = list.get(i);
            if ("ALIPAY".equals(payChannel.getChannelName())){
                if (fianceAliService.checkPayChannel(payChannel.getMchId(),billDate)){
                    channelList.add(payChannel);
                    _log.info("微信{}需要重新下载{}", payChannel.getMchId(), billDate);
                }
            } else if ("WX".equals(payChannel.getChannelName())){
                if (fianceWxService.checkPayChannel(payChannel.getMchId(),billDate)){
                    channelList.add(payChannel);
                    _log.info("支付宝{}需要重新下载{}", payChannel.getMchId(), billDate);
                }
            }
        }
        if (CollectionUtils.isEmpty(channelList)){
            _log.info("无需重新下载");
        }
        return channelList;
    }
}
