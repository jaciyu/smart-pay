package com.founder.fiance;

import com.founder.config.XxlJobConfig;
import com.founder.core.domain.FianceAl;
import com.founder.core.domain.FianceWx;
import com.founder.core.utils.DateUtil;
import com.xxl.job.core.log.XxlJobLogger;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class FianceTJ3ZX extends FianceBASE {

    @Autowired
    XxlJobConfig xxlJobConfig;

    @Override
    public String buildContent(List<?> list, String subsys) {

        if (CollectionUtils.isEmpty(list)) {
            XxlJobLogger.log("无数据：" + subsys);
            return null;
        }

        Object object = list.get(0);
        if (object instanceof FianceAl) {
            System.out.println("天津三中心支付宝");
            return buildContentAli(super.toFianceAlList(list), subsys);
        } else if (object instanceof FianceWx) {
            System.out.println("天津三中心微信");
            return buildContentWx(super.toFianceWxList(list), subsys);
        } else {
            return null;
        }
    }

    private String buildContentWx(List<FianceWx> list, String subsys) {

        String content = "";
        if ("MZ".equals(subsys)) {
            String regex = xxlJobConfig.getRegex_mz();
            System.out.println("匹配规则：" + regex);
            List<FianceWx> fianceWxList = list.stream()
                    .distinct()
                    .filter(x -> x.getBzorder().matches(regex))//"^ZZJ\\d\\d[1|2|3].*"
                    .collect(Collectors.toList());

            content = "";
            for (int i = 0; i < fianceWxList.size(); i++) {
                FianceWx fianceWx = fianceWxList.get(i);
                content = content
                        + StringUtils.right(DateUtil.date2Str(DateUtil.str2datetime(fianceWx.getTime()), "yyyyMMdd"),4) + "|"
                        + StringUtils.right(fianceWx.getTime().replace(":",""),6) + "|"
                        + fianceWx.getBzorder() + "|"
                        + (StringUtils.isBlank(fianceWx.getDeviceid()) ? "" : fianceWx.getDeviceid()) + "|"
                        + ("SUCCESS".equals(fianceWx.getTradestatus()) ? "00" : "02") + "|"
                        + (StringUtils.isBlank(fianceWx.getSubmch()) ? "" : fianceWx.getSubmch()) + "|"
                        + (StringUtils.isBlank(fianceWx.getWxrefundorder()) ? "" : fianceWx.getWxrefundorder()) + "|"
                        + ("SUCCESS".equals(fianceWx.getTradestatus()) ? String.format("%012d", fianceWx.getTotalmoney().multiply(new BigDecimal(100)).intValue()) : String.format("-%012d", fianceWx.getRefundmoney().multiply(new BigDecimal(100)).intValue())) + "|"
                        + "|"
                        + "|"
                        + fianceWx.getWxorder() + "|"
                        + "d|"
                        + "|"
                        + "\r\n";
            }
        } else if ("ZY".equals(subsys)) {
            String regex = xxlJobConfig.getRegex_zy();
            System.out.println("匹配规则：" + regex);
            List<FianceWx> fianceWxList = list.stream()
                    .distinct()
                    .filter(x -> x.getBzorder().matches(regex))//"^ZZJ\\d\\d[4].*"
                    .collect(Collectors.toList());

            content = "";
            for (int i = 0; i < fianceWxList.size(); i++) {
                FianceWx fianceWx = fianceWxList.get(i);
                content = content
                        + StringUtils.right(DateUtil.date2Str(DateUtil.str2datetime(fianceWx.getTime()), "yyyyMMdd"),4) + "|"
                        + StringUtils.right(fianceWx.getTime().replace(":",""),6) + "|"
                        + fianceWx.getBzorder() + "|"
                        + (StringUtils.isBlank(fianceWx.getDeviceid()) ? "" : fianceWx.getDeviceid()) + "|"
                        + ("SUCCESS".equals(fianceWx.getTradestatus()) ? "00" : "02") + "|"
                        + (StringUtils.isBlank(fianceWx.getSubmch()) ? "" : fianceWx.getSubmch()) + "|"
                        + (StringUtils.isBlank(fianceWx.getWxrefundorder()) ? "" : fianceWx.getWxrefundorder()) + "|"
                        + ("SUCCESS".equals(fianceWx.getTradestatus()) ? String.format("%012d", fianceWx.getTotalmoney().multiply(new BigDecimal(100)).intValue()) : String.format("-%012d", fianceWx.getRefundmoney().multiply(new BigDecimal(100)).intValue())) + "|"
                        + "|"
                        + "|"
                        + fianceWx.getWxorder() + "|"
                        + "d|"
                        + "|"
                        + "\r\n";
            }
        }

        System.out.println("对账文件内容。\r\n" + content);

        return content;
    }

    private String buildContentAli(List<FianceAl> list, String subsys) {

        String content = "";
        if ("MZ".equals(subsys)) {
            List<FianceAl> fianceAlList = list.stream()
                    .distinct()
                    .filter(x -> x.getBzorder().matches("^ZZJ\\d\\d[1|2|3].*"))
                    .collect(Collectors.toList());

            content = "";
            for (int i = 0; i < fianceAlList.size(); i++) {
                FianceAl fianceAl = fianceAlList.get(i);
                content = content
                        + DateUtil.date2Str(DateUtil.str2datetime(fianceAl.getTradetime()), "yyyyMMdd") + "|"
                        + StringUtils.right(fianceAl.getTradetime().replace(":",""),6) + "|"
                        + fianceAl.getBzorder() + "|"
                        + (StringUtils.isBlank(fianceAl.getTradeuser()) ? "" : fianceAl.getTradeuser()) + "|"
                        + ("交易".equals(fianceAl.getTradetype()) ? "00" : "02") + "|"
                        + (StringUtils.isBlank(fianceAl.getMchid()) ? "" : fianceAl.getMchid()) + "|"
                        + (StringUtils.isBlank(fianceAl.getTforder()) ? "" : fianceAl.getTforder()) + "|"
                        + ("交易".equals(fianceAl.getTradetype()) ? String.format("%012d", fianceAl.getTotalmoney().multiply(new BigDecimal(100)).intValue()) : String.format("%013d", fianceAl.getTotalmoney().multiply(new BigDecimal(100)).intValue())) + "|"
                        + "|"
                        + "|"
                        + (StringUtils.isBlank(fianceAl.getAlorder()) ? "" : fianceAl.getAlorder()) + "|"
                        + "e|"
                        + "|"
                        + "\r\n";
            }
        } else if ("ZY".equals(subsys)) {
            List<FianceAl> fianceAlList = list.stream()
                    .distinct()
                    .filter(x -> x.getBzorder().matches("^ZZJ\\d\\d[4].*"))
                    .collect(Collectors.toList());

            content = "";
            for (int i = 0; i < fianceAlList.size(); i++) {
                FianceAl fianceAl = fianceAlList.get(i);
                content = content
                        + DateUtil.date2Str(DateUtil.str2datetime(fianceAl.getTradetime()), "yyyyMMdd") + "|"
                        + StringUtils.right(fianceAl.getTradetime().replace(":",""),6) + "|"
                        + fianceAl.getBzorder() + "|"
                        + (StringUtils.isBlank(fianceAl.getTradeuser()) ? "" : fianceAl.getTradeuser()) + "|"
                        + ("交易".equals(fianceAl.getTradetype()) ? "00" : "02") + "|"
                        + (StringUtils.isBlank(fianceAl.getMchid()) ? "" : fianceAl.getMchid()) + "|"
                        + (StringUtils.isBlank(fianceAl.getTforder()) ? "" : fianceAl.getTforder()) + "|"
                        + ("交易".equals(fianceAl.getTradetype()) ? String.format("%012d", fianceAl.getTotalmoney().multiply(new BigDecimal(100)).intValue()) : String.format("%013d", fianceAl.getTotalmoney().multiply(new BigDecimal(100)).intValue())) + "|"
                        + "|"
                        + "|"
                        + "|"
                        + (StringUtils.isBlank(fianceAl.getAlorder()) ? "" : fianceAl.getAlorder()) + "|"
                        + "e|"
                        + "|"
                        + "\r\n";
            }
        }

        System.out.println("对账文件内容。\r\n" + content);

        return content;
    }
}
